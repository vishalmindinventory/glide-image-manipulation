/*
Add this file to app/Api/V1/Controllers
*/
<?php
namespace App\Api\V1\Controllers;

use App\Api\ApiController;
use Spatie\Glide\GlideImage;

class imageController extends ApiController
{
    public function getImage($folder, $img)
    {
        ## Check folder exists or not
        $imageFolder = storage_path('images/cache/'.$folder);
        if (!is_dir($imageFolder)) {
            mkdir($imageFolder, 0777, true);
            chmod($imageFolder, 0777);
        }

        $pathToImage = storage_path('images/'.$folder.'/'.$img);
        if (is_file($pathToImage)) {
            $width = \Request::get('w');
            $height = \Request::get('h');

            $tmpImageArr = (explode('.', $img));
            $ext = end($tmpImageArr);
            $image_name =  basename($img,'.'.$ext);
            $cacheImage = $image_name.'-'.(($width)?$width:0).'x'.(($height)?$height:0);
            $cacheImage = $cacheImage.'.'.$ext;

            $newImage = 'images/cache/'.$folder.'/'.$cacheImage;
            if (!\Storage::exists($newImage)) {
                GlideImage::create($pathToImage)
                    ->modify(['w'=> $width, 'h'=> $height])
                    ->save(storage_path($newImage));
            }
            return response()->file(storage_path($newImage));
        } else {
            return response()->file(\Storage::url('images/'.\Config::get('laravel-glide.default_images.default_user')));
        }
    }
}