# Please follow the instruction #

### Add below code to API route (DON'T MODIFY IT) ###

Add this code to top of the all routes.

```php
/***** DON'T DELETE THIS ROUTE ***/
// Glide image manipulate route
Route::get('/image/{folder}/{name}', 'imageController@getImage');
/***** END ***/
```

# Add [imageController.php] file to your API controller (e.g. app/Api/V1/Controllers/)

# How to use?

Define image url function in your Model file. (e.g. Users Models)
```php
public static function imageURL($img)
{
	return url('image/user/' . $img);
}
```
# Call function from resource file

```php
Users::imageURL("profile-101.jpg");
```
